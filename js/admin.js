
/**
 * Options with a grid view
 *
 */

(function($) {
    $.entwine(function($) {

       /**
	    * Class .tkimv-viewtemplateoptionset
	    */
		$('.tkimv-gridviewoptionset li').entwine({
			onclick: function() {
				// Find selected template name
				var tpl = $(this).find('input').prop('checked',true).val();
				this._super();
			}
		});

    });
})(jQuery);
