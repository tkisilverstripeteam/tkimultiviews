<?php

/**
 * Base view helper
 */
abstract class TkiView extends ViewableData implements TkiViewInterface {
	
	/**
	 * @config
	 * @var string
	 */
	private static $icon;

	/* 
	 * -------------------------------------------------------------------------
	 * Constructor / init
	 * -------------------------------------------------------------------------
	 */
	
	/**
	 * Return instance
	 * @return object
	 */
	public static function inst()
	{
		return Injector::inst()->create(get_called_class());
	}
	
	/**
	 * Overload in subclasses. 
	 * Contains logic to prepare view for rendering. Should be run after data 
	 * has been set on view.
	 * Run after setting properties on view.
	 */
	public function prepare() {}

	/* 
	 * -------------------------------------------------------------------------
	 * Getters / setters
	 * -------------------------------------------------------------------------
	 */
	
	/**
	 * Get translated name
	 */
	public function getViewIcon() {
		return $this->config()->get('icon');
	}
	
	/**
	 * Get translated name
	 */
	public function getViewTitle() {
		$class = get_class($this);
		return _t("{$class}.Title",$class);
	}
	
	/** 
	 * Get translated description
	 */
	public function getViewDescription() {
		$class = get_class($this);
		return _t("{$class}.Description",'');
	}
	
	/* 
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */
	
	/**
	 * Renders template
	 * @return HTMLText
	 */
	public function forTemplate()
	{
		return $this->renderWith($this->viewTemplates());
	}
	
	/**
	 * Possible templates for this object.
	 * Fails over to parent classes, not including TkiView.
	 * @return array
	 */
	public function viewTemplates()
	{
		$templates = array();
		// Failover to parent templates
		$ancestryClasses = array_reverse(ClassInfo::ancestry($this->class));
		$stopClasses   = ClassInfo::ancestry('TkiView');
		foreach($ancestryClasses as $class) {
			if(in_array($class,$stopClasses)) break;
			$templates[] = $class;
		}
		return $templates;
	}
	
	public function ViewClass()
	{
		return Convert::raw2att($this->class);
	}
	
}
