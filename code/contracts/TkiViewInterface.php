<?php

interface TkiViewInterface
{
	
	/**
	 * Return instance
	 * @return TkiViewInterface
	 */
	public static function inst();
	
	/**
	 * Contains logic to prepare view for rendering. Should be run after data 
	 * has been set on view.
	 * Run after setting properties on view.
	 * @return void
	 */
	public function prepare();

	/* 
	 * -------------------------------------------------------------------------
	 * Getters / setters
	 * -------------------------------------------------------------------------
	 */
	
	
	/**
	 * Gets icon path
	 * @return string
	 */
	public function getViewIcon();
	
	
	/**
	 * Gets translated title
	 * @return string
	 */
	public function getViewTitle();
	
	/** 
	 * Gets translated description
	 * @return string
	 */
	public function getViewDescription();
	
	/* 
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */
	
	/**
	 * Possible templates for this object.
	 * May fail over to parent classes
	 * @return array
	 */
	public function viewTemplates();
	
	/**
	 * Gets class name - for template use
	 * @return string
	 */
	public function ViewClass();
	

}

