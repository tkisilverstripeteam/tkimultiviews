<?php


class TkiMultiViewsExtension extends DataExtension
{

	/**
	 * @var array 
	 */
	private static $db = array(
		'ViewClass' => 'Varchar'
	);
	
	/**
	* Different views available for user selection.
	* Simple associative array with class name as key and icon for the value
	* @var array
	* @config
	*/
	private static $views = array();

    
    private static $hide_views = false;
    
	/**
	* Default view class
	* @var string
	* @config
	*/
	private static $default_view;

	/**
	* Template include name used for rendering each template option
	* @config
	* @var string
	*/
	private static $view_option_template = 'TkiMultiViewsGridViewOption';

    /**
	* @todo
	* @config
	* @var string
	*/
	//private static $view_settings_columns;
    
	/**
	* Instance cache of multi view object
	* @var array
	*/
	protected $multiView;
  
    
	/* 
	 * -------------------------------------------------------------------------
	 * Getters / setters
	 * -------------------------------------------------------------------------
	 */
	
	/**
	* Get $views configured for the class - uninherited 
	* @return array
	*/
	public function getAvailableViews()
	{
		return Config::inst()->get(get_class($this->owner),'views',Config::UNINHERITED);
	}
    
    /**
	* Get $views configured for the class - uninherited 
	* @return array
	*/
	public function getHideViews()
	{
		return Config::inst()->get(get_class($this->owner),'hide_views',Config::UNINHERITED);
	}
	
	/**
	 * Get $default_view configured for the class (should not inherit value)
	 * @return string
	 */
	public function getDefaultView()
	{
		return Config::inst()->get(get_class($this->owner),'default_view',Config::UNINHERITED);
	}

	/**
	 * Get view option template
	 * @return string
	 */
	public function getViewOptionTemplate()
	{
		return Config::inst()->get(get_class($this->owner),'view_option_template');
	}
    
	
    /**
	 * Get view settings fields
	 * @return string
	 
	public function getViewSettingsColumns()
	{
		return Config::inst()->get(get_class($this->owner),'view_settings_columns');
	}
    */
	
	/**
	 * Sets data on view
	 * Override in owner class to set view data
	 */
	public function setViewData($view) {}
	
	/**
	 * Gets selected view for rendering
	 * @return TkiViewInterface
	 */
	public function getSelectedView()
	{
		if(!$this->multiView) {
			$this->multiView = $this->initView($this->owner->ViewClass);
		}
        
		return $this->multiView;
	}
  
	/**
	 * Gets selected view or default view
	 * @return TkiViewInterface
	 */
	public function getMultiView()
    {
        if($this->multiView) {
           return $this->multiView; 
        }
        $view = $this->getSelectedView();
        if(!$view) {
            $class = $this->getDefaultView();
            $view = $this->initView($class);
        }
        
        $this->multiView = $view;
        return $this->multiView;
    }
    
    /**
     * Instantiate and prepare view
     * @param string $class
     * @return TkiViewInterface
     */
    protected function initView($class)
    {
        $view = (!empty($class) && class_exists($class)) ? Injector::inst()->create($class) : null;
        
        if($view) {
			$view->setFailover($this->owner);
	
			// Set data on view
			$this->owner->setViewData($view);
			
			// Run view logic if necessary
			$view->prepare();
		}
        
        return $view;
    }
    
	/*
	 * -------------------------------------------------------------------------
	 * Admin methods
	 * -------------------------------------------------------------------------
	 */
	
	public function updateCMSFields(FieldList $fields)
	{
		/*
		 * Requirements
		 */

		Requirements::css(TKIMULTIVIEWS_DIR .'/css/admin.css');
		Requirements::javascript(TKIMULTIVIEWS_DIR .'/js/admin.js');
		
		$fields->removeByName('ViewClass');
		/*
		 * Layouts
		 */
		$views = $this->getAvailableViews();
		$default = $this->getDefaultView();

		// If different views are available, then create options
		if($views && !$this->getHideViews()) {
			$options = array();
			$optionTpl = $this->getViewOptionTemplate();
			foreach($views as $class) {
				$view = class_exists($class) ? Injector::inst()->create($class) : null;

				// Skip invalid views
				if(!$view || !($view instanceof TkiView)) {
					continue;
				}

				/** @todo Hide radio buttons ? */
				// Prepare and render layout title and description
				$optionData = ArrayData::create(array(
					'Icon' => $view->getViewIcon(),
					'Title' => $view->getViewTitle(),
					'Description' => $view->getViewDescription()
				));
				$options[$class] = $optionData->renderWith($optionTpl);
			}

			// Appearance tab
			$appearanceTab = Tab::create('Appearance',_t('TkiMultiViewsExtension.AppearanceTab','Appearance'));
			$fields->insertAfter('Main',$appearanceTab);


			// Options field for ViewClass property
			$layoutField = OptionsetField::create('ViewClass',_t('TkiMultiViewsExtension.ViewClass','Layout'),$options,$default)->addExtraClass('tkimv-gridviewoptionset');
			$layoutField->setHasEmptyDefault(false);
			$fields->addFieldToTab('Root.Appearance',$layoutField);
            
            /*
             * View settings
             
            $columns = $this->getViewSettingsColumns();
            if(is_array($columns) && !empty($columns)) {
                $settingsFields = array();
                foreach($columns as $column) {
                    $field = $fields->dataFieldByName($column);
                    $field->setTitle = _t(get_class($this->owner).'.ViewSettings','Settings');
                    $fields->removeByName($column);
                    if($field) {
                        $settingsFields[] = $field;
                    }
                }
                $settingsTitle = HeaderField::create('ViewSettings',_t('TkiMultiViewsExtension.ViewSettings','Settings'));
                $fields->insertAfter('ViewClass',$settingsTitle);
                $fields->insertAfter('ViewSettings',CompositeField::create($settingsFields));
            }
            */
             
		}
		
	}

	public function onBeforeWrite()
	{
		// Check ViewClass jives with available layouts, in case available layouts or
		// view class has changed
		if($this->owner->ViewClass) {
			$views = $this->getAvailableViews();
			if(empty($views) || (is_array($views) && !in_array($this->owner->ViewClass, $views))) {
				$this->owner->ViewClass = null;
			}
		}

	}
	
	/*
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */
	
	/**
	 * View name for use in template
	 * @return type
	 */
	public function ViewClass()
	{
		return Convert::raw2att($this->owner->ViewClass);
	}
	
	/**
	 * Gets the max width configured or calculated (pixels)
	 * @return int
	 */
	public function getMaxWidth()
	{
		return (int) Config::inst()->get(get_class($this->owner),'max_width',Config::INHERITED);
	}

	/**
	 * Gets the max height configured or calculated (pixels)
	 * @return int
	 */
	public function getMaxHeight()
	{
		return (int) Config::inst()->get(get_class($this->owner),'max_height',Config::INHERITED);
	}
    
    /*
      
    protected function getViewSetting($name)
    {
        $settings = $this->getViewSettingsAsArray();
        return (isset($settings[$name]))
            ? $settings[$name]
            : null;
    }
     * 
    public function setViewSettings($value)
    {
        $this->owner->setField('ViewSettings',json_encode($value,JSON_FORCE_OBJECT));
    }
    
    public function getViewSettings()
    {
        return $this->owner->getField('ViewSettings');
    }
    
    public function getViewSettingsAsArray()
    {
        return json_decode($this->owner->getField('ViewSettings'), true);
    }

    */
}
